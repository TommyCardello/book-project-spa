'use strict';

angular.module('myApp.genres', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/genres', {
    templateUrl: 'genres/genres.html',
    controller: 'genInfoCtrl'
  });
}])


//Вывод авторов по жанрам.
 app.controller('authorsByGenreCtrl',[ '$scope', 'books', function($scope, books) {
       $scope.genres=books.allGenres();
       // console.log($scope.genres);
       $scope.authorsByGenre=function(genre){
       return books.authorsByGenre(genre)
         }
         console.log($scope.genres)
 }]);

 //Подгрузка деталей конкретного жанра.
 app.controller('genreDetailCtrl',[ '$scope', '$routeParams', 'books', function($scope, $routeParams, books) {
     $scope.books = books.genreById($routeParams.genreId);
     // console.log($scope.books);
 }]);

//Вывод книг по жанру
app.controller('booksByGenreCtrl',[ '$scope', 'books', function($scope, books) {
      $scope.booksByGenre=books.booksByGenre($scope.books.genre);
}]);

//Вывод авторов в данном жанре. 
app.controller('authorInGenreCtrl',[ '$scope', 'books', function($scope, books) {
      $scope.authorByGenre=books.authorByGenre($scope.books.genre);
}]);
